### About

This is cocoapods for Zoom IOS SDK. 

Pod includes several architectures:
(Device and Emulator)
Architectures may vary between pods.


Pod includes sdk list:
- MobileRTC.framework
- MobileRTCScreenShare.framework
- MobileRTCResources.bundle

### Usage
Update your Podfile:
```
pod 'ZoomSDK', '5.12.2.4959'
```


### Versions

- 5.12.2.4959
- 5.10.3.3244


